EasyBus

#Version 1.02 version code 3
1.Hoàn thành hiển thị danh sách tuyến buýt dữ liệu Hà Nội, Hồ Chí Minh
2.Hiện thị đầy đủ thông tin tuyến buýt, bến đố trên bản đồ
3.Chế độ tìm đường
4.Tìm bến đố gần bạn
5.Thông tin ứng dụng

# Version 1.03 version code 4
1.Tắt chế độ xoay màn hình
2.Sửa lại thoát bằng nút back
3.Chế độ hiện thị đường chính xác khi sử dụng kết nối mạng
4.Lỗi exception khi hiển thị tìm đường

# Version 1.04 version code 5
1.Exception SQLite
2.Báo lỗi cơ bản (tuyến khong hoạt động + sửa thông tin)
3.Chỉnh lại back event